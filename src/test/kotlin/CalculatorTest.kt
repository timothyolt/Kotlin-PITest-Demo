package com.timothyolt.mutant

import org.junit.Test
import kotlin.test.assertEquals

class CalculatorTest {
    @Test
    fun testAddition() {
        val calculator = Calculator()
        calculator.add(2.0)
        assertEquals(2.0, calculator.getResult())
    }

    @Test
    fun testPower() {
        val calculator = Calculator(displayedValue = 2.0)
        calculator.power(3.0)
        assertEquals(8.0, calculator.getResult())
    }

    @Test
    fun testConditionalSetTrue() {
        val calculator = Calculator()

        assertEquals(true, calculator.tryToSetDisplay(2.0, true))
    }

    @Test
    fun testConditionalSetFalse() {
        val calculator = Calculator()
        assertEquals(false, calculator.tryToSetDisplay(2.0, false))
    }
}
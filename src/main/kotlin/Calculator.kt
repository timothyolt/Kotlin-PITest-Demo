package com.timothyolt.mutant

import kotlin.math.pow

class Calculator(var displayedValue: Double = 0.0) {

    // Operations over the displayed value

    fun add(x: Double) {
        displayedValue += x
    }

    fun subtract(x: Double) {
        displayedValue -= x
    }

    fun power(x: Double) {
        displayedValue = displayedValue.pow(x)
    }

    // Offers to the client the current value
    fun getResult() = displayedValue

    // This function serves to update the current value in the displayed itself.
    // The client can allow the change or not.
    // The examplification purpose of this function will be clear in the rest of the post.
    fun tryToSetDisplay(x: Double, canChangeDisplay: Boolean): Boolean {
        return if (canChangeDisplay) {
            setDisplay(x)
            true
        } else {
            false
        }
    }

    private fun setDisplay(x: Double) {
        displayedValue = x
    }
}